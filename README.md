My Name is maduka okwuchukwu, mobile developer, 
this project is developed with flutter framework and dart programing language.
the project has seven (7) API integration as follows,
 SIGNIN, 
 SIGNUP, 
 Transaction, 
 Authenticated Users, 
 Withdraw,
 Transfer, and 
 Account List.

Once you install the app And run, the first page your going to see is SIGNIN page, if your a new user you can navigate to sign up page.

After successfull signIn Or SignUp then you will be redirected to the home page, where you will see the summary of what is happening in the app. the user profile picture in the app is handcoded also, the account balance and the account number in the home page is handcoded.

CHALLENGES

the Transaction API has a problem from the backend, some of the list of data has "Balance" parameter, while some dont have it, is returning Error, null error, that's while, the circular indicator keep rotataing on the home page, under Recent transaction and Transaction History Page.

There is problem in sign in registered user, the problem is from the back-end also, after a user successfull signup and wanted to login again, with the sign up credentials, initially it was working at a point it stoped, please check your back-end for the problem.

Link to the Apk is here, hosted on Google Drive: https://drive.google.com/file/d/10R1BLzf4AzjjMkRQSgDqKbYH5GNsKJFk/view?usp=sharing
